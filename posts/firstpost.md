---
title: Hello World!
date: 2019-05-27T23:00:00.000Z
summary: It's alive!
---
Hi!

This thing is alive! I am so happy! 

I have been gone in academic-research-land for way too long. At this point, I just had to release _something_.

It's unfinished, unpolished, and un-**everything**. But it's a start. 

Welcome to Akos-land!
